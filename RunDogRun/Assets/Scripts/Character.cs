﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{
    // Character Behaviour Variables
    public enum NormalStates
    {
        Idle,
        Jumping,
        Running,
        Tired,
        Stopped,
        Walking,
        Fighting
    }
    public enum SpecialStates
    {
        None,
        Radioactivity
    }

    private float timeLastState;
    private float timeLastSpecial;
    public float LifeTimeStarts { get; private set; }

    public NormalStates StatePrev { get; private set; }
    public bool IsWalking { get; private set; }
    public bool IsRunning{ get; private set; }
    private bool alive;
    public bool Alive
    {
        get { return this.alive; }
        private set
        {
            if (value)
            {
                this.StateActual = NormalStates.Idle;
                this.StateSpecial = SpecialStates.None;
                this.Stamina = MaxStamina;
                this.LifeTimeStarts = Time.time;
                this.StartDistance = transform.position.z;
                this.BallsCollected = 0;
            }
            this.alive = value;
        }
    }
    private NormalStates stateActual;
    public NormalStates StateActual
    {
        get { return this.stateActual; }
        set
        {
            if (value == NormalStates.Fighting) this.toFigth = true;
            if (value != this.StateActual)
            {
                this.timeLastState = Time.time;
                this.StatePrev = this.stateActual;
                this.stateActual = value;
            }
        }
    }

    private SpecialStates stateSpecial;
    private SpecialStates StateSpecial
    {
        get { return this.stateSpecial; }
        set
        {
            float impactValue = 0f;
            if (value == SpecialStates.Radioactivity && value == this.stateSpecial )
            {
                float probability = Random.Range(1, 3);
                impactValue = Random.Range(0, 50);
                if (probability > 2) impactValue = -impactValue;
                else if (probability == 2) impactValue = 0;
            }
            this.ImpactToStamina(impactValue);
            this.timeLastSpecial = Time.time;
            this.stateSpecial = value;
        }
    }

    public float Stamina { get; private set; }
    public float MaxStamina { get; } = 100;
    public int BallsCollected { get; private set; }
    public float StartDistance { get; private set; }
    public string userName;
    public float Speed { get; private set; }
    public float SpeedRunning { get; private set; }

    // Trigger Actions Variables1
    private bool toJump;
    private bool toRun;
    private bool toWalk;
    private bool toStop;
    private bool toFigth;
    private bool touchingFloor;

    // GameObject Variables
    private Rigidbody mainRigidbody;
    public Collider triggerCollider;

    public class Stats
    {
        public float stamina;
        public int ballsCollected;
        public float distance;
        public float timePlaying;
        public string username;
    }

    public Stats stats;

    private Animator animator;
    private Light radioactiveLight;
    // Start is called before the first frame update
    void Start()
    {
        stats = new Stats();
        radioactiveLight = GetComponentInChildren<Light>();
        radioactiveLight.enabled = false;
        animator = GetComponentInChildren<Animator>();
        mainRigidbody = GetComponent<Rigidbody>();
        Speed = 0.25f;
        SpeedRunning = 1.25f;
        userName = "";
        SetDefaultValues();
        transform.position.Set(
            transform.position.x,
            transform.position.y,
            StartDistance
        );
    }

    // Update is called once per frame
    private void Update()
    {
        if (!this.toFigth && this.alive)
        {
            AssignInputs();
            animator.SetBool("standing", StateActual == NormalStates.Stopped || StateActual == NormalStates.Idle );
            animator.SetBool("walking", StateActual == NormalStates.Walking);
            animator.SetBool("running", StateActual == NormalStates.Running );
            animator.SetBool("touchingFloor", touchingFloor );
        }
        else if (this.toFigth)
        {

        }
    }

    private void FixedUpdate()
    {
        if (!this.toFigth && this.alive)
        {
            this.TriggersPhysics();
        }
        else if (this.toFigth)
        {

        }
    }

    public void UpdateStats(Stats newStats)
    {
        stats = newStats;
    }

    void AssignInputs()
    {
        if ((Input.GetButtonDown("Jump") || Input.GetButtonDown("Vertical"))
            && touchingFloor
            && StateActual != NormalStates.Jumping
        )
        {
            this.toJump = true;
        }
        if (Input.GetButton("Horizontal"))
        {
            this.toWalk = true;
            IsWalking = true;
        }
        if (Input.GetButtonDown("Run") && this.toWalk)
        {
            this.toRun = true;
            IsRunning = true;
            IsWalking = false;
        }
        if (Input.GetButtonUp("Run") || !this.toWalk)
        {
            this.toRun = false;
            IsRunning = false;
        }
        if (Input.GetButtonUp("Horizontal"))
        {
            IsWalking = false;
        }
        toStop = !toWalk;
    }

    private void TriggersPhysics()
    {
        Vector3 newPosition = mainRigidbody.position;

        if (toJump && touchingFloor)
        {
            int impulseForce = 100;   
            if( this.StateSpecial == SpecialStates.Radioactivity && Random.Range(-2, 1) >= 0 ){
                impulseForce = Random.Range(50, 200);
            }
            mainRigidbody.AddRelativeForce(force: (transform.up) * Time.fixedDeltaTime * impulseForce, mode: ForceMode.Impulse);
            ImpactToStamina(-1);
            animator.SetTrigger( "jumping" );
            this.StateActual = NormalStates.Jumping;
        }
        else if (toStop)
        {
            this.StateActual = NormalStates.Stopped;
        }
        else if (toRun)
        {
            ImpactToStamina(-0.075f);
            newPosition.z += SpeedRunning * Time.fixedDeltaTime;
            this.StateActual = NormalStates.Running;
        }
        else if (toWalk)
        {
            newPosition.z += Speed * Time.fixedDeltaTime;
            ImpactToStamina(-0.05f);
            this.StateActual = NormalStates.Walking;
        }

        mainRigidbody.position = newPosition;
        EraseActions();
    }

    private void CreateEffects()
    {
        if (this.StateSpecial == SpecialStates.Radioactivity)
        {
            // Radioactive effect like:
            // this.gameObject.GetComponent<SpriteRenderer>().color = green;
            // Start coroutine for change the specialState to None
        }
    }

    public void ImpactToStamina(float impactValue)
    {
        this.Stamina = this.Stamina + (impactValue);
        if (this.Stamina <= 0)
        {
            
            animator.SetBool("standing", true );
            this.Stamina = 0;
            this.Alive = false;
        }
        if (Stamina > MaxStamina)
        {
            Stamina = MaxStamina;
        }
    }

    public void SetDefaultValues()
    {
        this.Alive = true;
        EraseActions();
    }

    private void EraseActions()
    {
        this.toJump = false;
        this.toWalk = false;
        this.toStop = true;
        this.toFigth = false;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "pickup")
        {
            radioactiveLight.enabled = false;
            Destroy(other.gameObject);
            if (other.gameObject.name.StartsWith("bone"))
            {
                int staminaAfect = 3;
                if( this.stateSpecial == SpecialStates.Radioactivity ){
                    staminaAfect = Random.Range( -5, 10);
                } 
                ImpactToStamina( staminaAfect );
            }
            else if (other.gameObject.name.StartsWith("ball"))
            {
                BallsCollected++;
                if( this.stateSpecial == SpecialStates.Radioactivity && Random.Range(-3, 1) > 0 ){
                    BallsCollected++;
                }
            } 
            else if (other.gameObject.name.StartsWith("radioactive"))
            {
                ImpactToStamina(Random.Range(-15, 10));
                this.StateSpecial = SpecialStates.Radioactivity;
                StartCoroutine( BlinkRadiactive() );
            }
        }
        else
        {
            touchingFloor = true;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (mainRigidbody.velocity.normalized.y > 0) touchingFloor = false;
    }

    IEnumerator BlinkRadiactive() 
    {
        float duration = Time.time + 5f;

        while(Time.time <= duration)
        {
            radioactiveLight.enabled = true;
            radioactiveLight.intensity = (float) (( Time.time % 0.5 ) * 10);
            yield return null;
        }
        
        this.StateSpecial = SpecialStates.None;
        radioactiveLight.enabled = false;
    }
}
