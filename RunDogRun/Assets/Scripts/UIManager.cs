﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [System.Serializable]
    public class PlayerStatsUI
    {
        public GameObject root;
        public Slider staminaSlider;
        public TMPro.TextMeshProUGUI distanceText;
        public TMPro.TextMeshProUGUI ballsText;
        public TMPro.TextMeshProUGUI timeText;
    }
    [System.Serializable]
    public class PausedUI
    {
        public GameObject root;
        public TMPro.TextMeshProUGUI pauseText;
        public Button reanudarButton;
        public Button goToMenuButton;
    }

    [System.Serializable]
    public class EndUI
    {
        public GameObject root;
        public TMPro.TextMeshProUGUI distanceText;

        public TMPro.TextMeshProUGUI ballsText;
        public TMPro.TextMeshProUGUI timeText;
        public TMPro.TMP_InputField userName;

        public Button guardarRecordButton;
        public Button goToMenuButton;
    }
    public PausedUI pausedUI = new PausedUI();
    public EndUI endUI = new EndUI();
    public PlayerStatsUI statsUI = new PlayerStatsUI();
    public Character playerScript;
    // Start is called before the first frame update
    void Start()
    {
        setDefaultValues();
    }

    public void updateStats()
    {
        statsUI.staminaSlider.value = playerScript.stats.stamina;
        statsUI.distanceText.text = "Distance: " + float.Parse(playerScript.stats.distance.ToString("F1")) + " mts.";
        statsUI.ballsText.text = "Pelotas: " + playerScript.stats.ballsCollected;
        statsUI.timeText.text = "" + float.Parse(playerScript.stats.timePlaying.ToString("F1")) + " sec.";
        endUI.distanceText.text = float.Parse(playerScript.stats.distance.ToString("F1")) + " mts.";
        endUI.ballsText.text = "" + playerScript.stats.ballsCollected;
        endUI.timeText.text = statsUI.timeText.text;
    }

    public void setDefaultValues()
    {
        playerScript.SetDefaultValues();
        statsUI.staminaSlider.minValue = 0f;
        statsUI.staminaSlider.maxValue = playerScript.Stamina;
        statsUI.staminaSlider.value = playerScript.MaxStamina;
        statsUI.distanceText.text = "Distance: 0";
    }
    public void showPause(bool show = false)
    {
        pausedUI.root.SetActive(show);
    }

    public void showPlayerStats(bool show = true)
    {
        statsUI.root.SetActive(show);
    }
    public void showEnd(bool show = false)
    {
        endUI.root.SetActive(show);

        if (show)
        {
            string newUserName = endUI.userName.text;
            endUI.guardarRecordButton.interactable = !string.IsNullOrEmpty(endUI.userName.text);
        }
    }
}
