﻿using System.Collections.Generic;
using UnityEngine;

public class StreetsGenerator : MonoBehaviour
{
    [SerializeField]
    public GameObject[] prefabStreets;
    private List<GameObject> streetsRendered;
    private Transform streetsRoot;

    // Main Camera
    public Camera playerCamera;
    // Start is called before the first frame update
    void Start()
    {
        streetsRendered = new List<GameObject>();
        streetsRoot = GetComponent<Transform>();
        if (playerCamera == null)
        {
            playerCamera = Camera.main;
        }
        createDefaultStreetsWidth();
    }

    // Update is called once per frame
    void Update()
    {
        int positionZ = Mathf.RoundToInt(playerCamera.transform.position.z);
        int streetPositionZ = Mathf.RoundToInt(streetsRendered[2].transform.position.z);
        if (positionZ > streetPositionZ)
        {
            Destroy(streetsRendered[0]);
            streetsRendered.RemoveAt(0);
            createNewStreet(streetsRendered[streetsRendered.Count - 1].transform.position.z + 1);
        }
    }

    void createDefaultStreetsWidth()
    {
        if (prefabStreets.Length > 0)
        {
            for (int i = -2; i < 2; i++)
            {
                createNewStreet(i);
            }
        }
    }

    void createNewStreet(float positionZ)
    {
        int idxStreet = Random.Range(0, prefabStreets.Length);
        Vector3 newPosition = streetsRoot.position;
        GameObject newStreet;
        Collider[] obstacles;
        newPosition.z = newPosition.z + (positionZ);
        newStreet = Instantiate(prefabStreets[idxStreet], newPosition, Quaternion.identity, streetsRoot);
        obstacles = newStreet.GetComponentsInChildren<Collider>();
        
        foreach (Collider item in obstacles)
        {
            GameObject gameObject = item.gameObject;
            if( gameObject.tag == "Obstacle"){
                if( Random.Range(0, 6) != 0 ){
                    Destroy( gameObject );
                }
            }
        }
        streetsRendered.Add(newStreet);
    }
}
