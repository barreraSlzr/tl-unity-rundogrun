﻿using UnityEngine;

public class pickupObject : MonoBehaviour
{
    private Rigidbody rgbdy;

    [SerializeField]
    [Range(0f, 1f)]
    public float bouncingFactor;
    // Start is called before the first frame update
    void Start()
    {
        rgbdy = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y <= -1f) Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision other)
    {
        rgbdy.velocity = Vector3.Reflect(other.relativeVelocity * -(bouncingFactor), other.contacts[0].normal);
    }
}
