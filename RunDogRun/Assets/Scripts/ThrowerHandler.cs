﻿using System.Collections.Generic;
using UnityEngine;

public class ThrowerHandler : MonoBehaviour
{
    public GameObject[] prefabObjects;
    public GameObject pickUpsCollector;

    private List<GameObject> objectsThrowed;
    private bool newObject;
    public GameManager GameManager;

    float timeSinceLastItem = 0f;

    // Start is called before the first frame update
    void Start()
    {
        setDefaultValues();
        //transform.position = new Vector3(0, Screen.height / 2, -0.25f);
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.pausedGame && GameManager.PlayerIsAlive)
        {
            if (GameManager.PlayerIsRunning || GameManager.PlayerIsWalking)
            {
                timeSinceLastItem += Time.deltaTime;
                if (timeSinceLastItem >= 1f)
                {
                    newObject = true;
                    timeSinceLastItem = 0f;
                }
            }
            UpdatePosition();
        }
    }

    private void FixedUpdate()
    {
        deleteObjectsThrowed();
        if (newObject)
        {
            newObject = false;
            ThrowNewObject();
        }
    }

    void UpdatePosition()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = -0.25f;
        if (mousePosition.y < 0) mousePosition.y = 0;
        if (mousePosition.y > Screen.height) mousePosition.y = Screen.height;
        mousePosition.y = Screen.height / 2;
        Vector3 newPosition = transform.position;
        newPosition.y = Camera.main.ScreenToWorldPoint(mousePosition).y;
        //newPosition.y = Camera.main.ScreenToWorldPoint(mousePosition).y;
        newPosition.z = (Camera.main.transform.position.z + 0.5f);
        transform.position = newPosition;
    }

    void ThrowNewObject()
    {
        int idxNewObject = Random.Range(0, prefabObjects.Length);
        Vector3 newPosition = transform.position;
        GameObject newObjectThrowed;
        float impulse;
        if (GameManager.PlayerIsRunning)
        {
            impulse = Random.Range(0.15f, 0.22f);
        }
        else
        {
            impulse = Random.Range(0.22f, 0.28f);
        }
        Vector3 forceSource = Vector3.up - (Vector3.forward * Random.Range(0.25f, 0.35f));
        newObjectThrowed = Instantiate(prefabObjects[idxNewObject], newPosition, Quaternion.identity, pickUpsCollector.transform);
        newObjectThrowed.GetComponent<Rigidbody>().AddRelativeForce((forceSource * impulse), ForceMode.Impulse);
        objectsThrowed.Add(newObjectThrowed);
        deleteObjectsThrowed();
    }

    void deleteObjectsThrowed()
    {
        for (int i = 0; i < objectsThrowed.Count; i++)
        {
            if (objectsThrowed[i] == null)
            {
                objectsThrowed.RemoveAt(i);
            }
        }
    }

    void setDefaultValues()
    {
        newObject = false;
        objectsThrowed = new List<GameObject>();
    }
}
