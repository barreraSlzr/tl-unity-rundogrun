﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    public Camera playerCamera;
    private int ScreenSizeX = 0;
    private int ScreenSizeY = 0;
    bool showRecords;
    public Button goToRecordsButton;
    public Button goToGamePlayButton;

    public GameObject RecordsList;
    // Start is called before the first frame update
    void Start()
    {
        showRecords = false;
    }

    // Update is called once per frame
    void Update()
    {
        RescaleCamera();
        RecordsList.SetActive(showRecords);
    }

    public void onClickGoToGameplay()
    {
        SceneManager.LoadScene("GamePlay");
    }

    private void RescaleCamera()
    {
        // Posicion de botones responsivos al tamaño de la pantalla

        Vector2 newButtonsSize = new Vector2((Screen.width / 2) - (Screen.width / 4), (Screen.height / 6));
        RectTransform rectTransform = goToGamePlayButton.GetComponent<RectTransform>();
        Vector2 newButtonsPosition = rectTransform.anchoredPosition;

        newButtonsPosition.x = (Screen.width / 12);

        if (showRecords)
        {
            newButtonsPosition.y = (Screen.height / 2) - newButtonsSize.y;
            goToGamePlayButton.GetComponent<RectTransform>().anchoredPosition = newButtonsPosition;

            newButtonsPosition.y = newButtonsSize.y - (Screen.height / 2);
            goToRecordsButton.GetComponent<RectTransform>().anchoredPosition = newButtonsPosition;
        }
        else
        {

            newButtonsPosition.y = newButtonsSize.y;
            goToGamePlayButton.GetComponent<RectTransform>().anchoredPosition = newButtonsPosition;

            newButtonsPosition.y = -newButtonsSize.y;
            goToRecordsButton.GetComponent<RectTransform>().anchoredPosition = newButtonsPosition;
        }

        // Termina la funcion si el tamaño de la pantalla no ha cambiado
        if (Screen.width == ScreenSizeX && Screen.height == ScreenSizeY) return;

        float targetaspect = 16.0f / 9.0f;
        float windowaspect = (float)Screen.width / (float)Screen.height;
        float scaleheight = windowaspect / targetaspect;


        if (scaleheight < 1.0f)
        {
            Rect rect = playerCamera.rect;

            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;

            playerCamera.rect = rect;
        }
        else // add pillarbox
        {
            float scalewidth = 1.0f / scaleheight;

            Rect rect = playerCamera.rect;

            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;

            playerCamera.rect = rect;
        }

        ScreenSizeX = Screen.width;
        ScreenSizeY = Screen.height;

        // Tamaño de botones responsivos al tamaño de la pantalla

        // Tamaño de botones
        goToGamePlayButton.GetComponent<RectTransform>().sizeDelta = new Vector2(newButtonsSize.x, newButtonsSize.y);
        goToRecordsButton.GetComponent<RectTransform>().sizeDelta = new Vector2(newButtonsSize.x, newButtonsSize.y);
    }

    public void onClickShowRecordsHandler()
    {
        Text recordsButtonText = goToRecordsButton.GetComponentInChildren<Text>();
        showRecords = !showRecords;

        recordsButtonText.text = (showRecords) ? "Atras" : "Mostrar Records";
    }

    void showRecordsList()
    {
    }
}
