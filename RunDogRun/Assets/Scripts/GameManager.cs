﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    public Camera playerCamera;
    private int ScreenSizeX = 0;
    private int ScreenSizeY = 0;

    public GameObject streets;

    public GameObject player;
    private float diffPositionZ;

    #region PlayerStats
    private Character playerScript;
    public bool PlayerIsWalking { get { return playerScript.IsWalking;} }
    public bool PlayerIsRunning { get { return playerScript.IsRunning;} }
    public bool PlayerIsAlive{ get { return playerScript.Alive;} }
    #endregion

    #region GameSettings
    private List<Character.Stats> RecordHistory = new List<Character.Stats>();
    private bool toPauseGame;
    private bool ToPauseGame
    {
        get { return toPauseGame; }
        set
        {
            toPauseGame = value;
            pausedGame = toPauseGame;
            if (toPauseGame) Time.timeScale = 0;
            else Time.timeScale = 1;
        }
    }
    public bool pausedGame { get; private set; }
    #endregion

    public UIManager gameInterface;
    // Start is called before the first frame update
    void Start()
    {
        ToPauseGame = false;
        if (playerCamera == null)
        {
            playerCamera = Camera.main;
        }
        playerScript = player.GetComponent<Character>();
        diffPositionZ = player.transform.position.z - playerCamera.transform.position.z;
        RescaleCamera();
    }

    // Update is called once per frame
    void Update()
    {
        RescaleCamera();

        gameInterface.showPause(pausedGame);
        gameInterface.showEnd(!playerScript.Alive);
        gameInterface.showPlayerStats( playerScript.Alive );

        if (!pausedGame && playerScript.Alive)
        {
            assingInputs();
            updatePlayerStats();
            Vector3 newCameraPosition = new Vector3(
                playerCamera.transform.position.x,
                playerCamera.transform.position.y,
                (player.transform.position.z - diffPositionZ));
            playerCamera.transform.position = newCameraPosition;
        }
    }

    private void assingInputs()
    {
        if (Input.GetButtonDown("Pause"))
        {
            ToPauseGame = !ToPauseGame;
        }
    }

    void updatePlayerStats()
    {
        Character.Stats newStats = playerScript.stats;
        playerScript.stats.distance = (player.transform.position.z - playerScript.StartDistance);
        playerScript.stats.stamina = playerScript.Stamina;
        playerScript.stats.ballsCollected = playerScript.BallsCollected;
        playerScript.stats.timePlaying = Time.time - playerScript.LifeTimeStarts;

        playerScript.UpdateStats(newStats);
        gameInterface.updateStats();
    }

    public void onClickToPauseGame()
    {
        ToPauseGame = !ToPauseGame;
    }

    public void saveNewRecord()
    {
        playerScript.userName = gameInterface.endUI.userName.text;
    }

    public void onClickToMenuGame()
    {
        SceneManager.LoadScene("GameMenu");
    }

    private void RescaleCamera()
    {
        if (Screen.width == ScreenSizeX && Screen.height == ScreenSizeY) return;

        float targetaspect = 16.0f / 9.0f;
        float windowaspect = (float)Screen.width / (float)Screen.height;
        float scaleheight = windowaspect / targetaspect;

        if (scaleheight < 1.0f)
        {
            Rect rect = playerCamera.rect;

            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;

            playerCamera.rect = rect;
        }
        else // add pillarbox
        {
            float scalewidth = 1.0f / scaleheight;

            Rect rect = playerCamera.rect;

            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;

            playerCamera.rect = rect;
        }

        ScreenSizeX = Screen.width;
        ScreenSizeY = Screen.height;
    }
}
