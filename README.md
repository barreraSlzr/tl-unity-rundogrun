-----POM RUN 1.0----  
22/April/2019 - Guadalajara, Jalisco, México.  

 	  		
| Rol | Fantastic Person | Contact |
| ------ | ------ | ------ |
| 2D Arts and Animation: | **Itzel Perez** | itzel_estefy@hotmail.com | 
| 3D Modelations and Mockups: | **Samanta Michel** | s.michel.torres@gmail.com |   
| Coding: | **Emmanuel Barrera** | barreraslzr@gmail.com | 
| Music and sound effects: | **Luis Ruvalcaba** | luis.ruvalcaba.limon@gmail.com |   
  
This repository was uploaded for you to learn how to use Unity.  
Please, do not profit with this project.  