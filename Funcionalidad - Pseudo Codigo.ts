interface ICharacter {
    states: {
        actual: 'Jumping' | 'Running' | 'Tired' | 'Stopped' | 'Walking' | 'Fighting',
        prev: ICharacter['states']['actual'],
        special: 'Radioactivity',
        initTimeLastState,
        initTimeLastSpecial
    },
    stats: {
        stamina: number,
        distance: number,
        timingAlive: any
    },
    movements: {
        toJump: boolean,
        toEat: boolean,
        toRun: boolean,
        toWalk: boolean,
        toStop: boolean,
        toFigth: boolean
    }
};

interface IEffect {
    value: number,
    makeState: ICharacter['states']['actual'] | ICharacter['states']['special'];
}
interface IPickUpType {
    type: string,
    effect: IEffect,
    prefab?: any
}

const pickUpsObjects: IPickUpType[] = [
    { 
        type: 'boneRadiactived', 
        effect: { 
            value: ( parseFloat( Math.random().toFixed(2) ) * 100) > 75 ? ( parseFloat( Math.random().toFixed(2) ) * 100) : -( parseFloat( Math.random().toFixed(2) ) * 100), 
            makeState: 'Radioactivity' }
    },
    { 
        type: 'bonePlastic', 
        effect: { 
            value: ( parseFloat( Math.random().toFixed(2) ) * 100) % 20,
            makeState: null }
    },
    { 
        type: 'boneNormal', 
        effect: { 
            value: ( ( parseFloat( Math.random().toFixed(2) ) * 100) % 15 ) + 15,
            makeState: null }
    },
    { 
        type: 'catAngry', 
        effect: { 
            value: -( parseFloat( Math.random().toFixed(2) ) * 100) % 20, 
            makeState: 'Fighting' }
    },
    { 
        type: 'ballNormal', 
        effect: { 
            value: ( parseFloat( Math.random().toFixed(2) ) * 100) % 20, 
            makeState: null }
    },
    { 
        type: 'ballRadiactived', 
        effect: { 
            value: ( parseFloat( Math.random().toFixed(2) ) * 100) > 25 ? ( parseFloat( Math.random().toFixed(2) ) * 100) % 20 : -(( parseFloat( Math.random().toFixed(2) ) * 100) % 20),
            makeState: 'Radioactivity' }
    },
];

interface gameStats {
    actual: {
        pause: boolean,
        distance: number,
        timing: any
    },
    history: ICharacter['stats'] [],
    config: {
        difficulty: 'hard' | 'normal' | 'easy',
    }  

}


const Setup = () => {
    assignDefaultCharacterValues();
    assignDefaultGameValues();
    createFloor();
    showMenu();
}

const Update = () => {
    checkWidthFloor();
    makeGamerInputs();
}

const FixedUpdate = () => {
    checkGameStats();
    checkGamerInputs();
}